import styles from "./Button.module.scss";

const Button = ({ label, isOutlined = false, style }) => {
    return (
        <button style={style} className={isOutlined ? styles.buttonOutlined : styles.buttonPrimary}>
            {label}
        </button>
    );
};

export default Button;
