import styles from "./PlayToEarn.module.scss";
import Button from "../Button/Button";
import Image from "next/image";

const PlayToEarn = ({ labelWhite, labelOrange, description, buttonLabel, rightChildComponent }) => {
    return (
        <div className={styles.playToEarnContainer}>
            <div className={styles.playToEarn}>
                <div className={styles.leftChild}>
                    <Button label={buttonLabel} />
                    <h1>
                        {labelWhite}
                        <span> {labelOrange}</span>
                    </h1>
                    <p>{description}</p>
                </div>
                <div className={styles.rightChild}>{rightChildComponent}</div>
            </div>
        </div>
    );
};

export default PlayToEarn;
