import Button from "../Button/Button";
import styles from "./Form.module.scss";

const Form = () => {
    return (
        <div className={styles.formContainer}>
            <input type="text" name="name" id="name" placeholder="Full Name" />
            <input type="email" name="email" id="email" placeholder="Email" />
            <input type="tel" name="tel" id="tel" placeholder="Phone Nummber" />
            <Button
                label={"Submit"}
                style={{
                    border: "none",
                    padding: "10px 20px",
                }}
            />
        </div>
    );
};

export default Form;
