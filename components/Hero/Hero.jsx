import Link from "next/link";
import Button from "../Button/Button";
import styles from "./Hero.module.scss";
import { MAIN_LINK } from "@/constants";
import Image from "next/image";

const Hero = ({ isHomePage = false, label }) => {
	return (
		<div className={styles.hero}>
			<Link href="/">
				<Image className={styles.roundedLogo} src="/assets/logo-master.jpg" width={150} height={150} />
			</Link>
			<div className={styles.heroTextGroup}>
				{isHomePage ? (
					<p>
						Win Real Money Rewards With <br />{" "}
						<span className={styles.heroTextOrange}>Teen Patti Master</span>{" "}
					</p>
				) : (
					<p>{label}</p>
				)}
			</div>
			<div className={styles.heroButtonGroup}>
				<Link href={MAIN_LINK}>
					<Button label={"Download Now"} />
				</Link>
			</div>
		</div>
	);
};

export default Hero;
