import Image from "next/image";
import styles from "./StepsCard.module.scss";
import ArrowDownIcon from "../../public/assets/icon.svg";

const StepsCard = ({ heading, boldText, stepNumber, description }) => {
    return (
        <div className={styles.cardContainer}>
            <div className={styles.header}>
                <Image
                    style={{
                        alignSelf: "flex-start",
                    }}
                    src={ArrowDownIcon}
                    width={30}
                    height={30}
                />
                <span>{stepNumber}</span>
            </div>
            <div>
                <div>
                    <h3>{heading}</h3>
                </div>
                <div>
                    <p>
                        <span className={styles.boldText}>{boldText}</span> <span>{description}</span>
                    </p>
                </div>
            </div>
        </div>
    );
};

export default StepsCard;
