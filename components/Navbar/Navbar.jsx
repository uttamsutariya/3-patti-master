"use client";
import Image from "next/image";
import Link from "next/link";
import styles from "./Navbar.module.scss";
import Button from "../Button/Button";
import { RxHamburgerMenu } from "react-icons/rx";
import { CgClose } from "react-icons/cg";

import { usePathname } from "next/navigation";
import { useEffect, useState } from "react";
import { MAIN_LINK } from "@/constants";

const Navbar = () => {
    const path = usePathname();
    const [isNavOpen, setIsNavOpen] = useState(false);

    useEffect(() => {
        setIsNavOpen(false);
    }, [path]);

    return (
        <div className="wrapper">
            <div className={styles.navContainer}>
                <Link href="/">
                    <Image className={styles.roundedLogo} src="/assets/logo-master.jpg" width={50} height={50} />
                </Link>
                <div className={styles.navLinks}>
                    <Link className={path === "/" ? styles.navItemActive : styles.navItem} href="/">
                        Home
                    </Link>
                    <Link className={path === "/how-to-play" ? styles.navItemActive : styles.navItem} href="/how-to-play">
                        How To Play
                    </Link>
                    <Link className={path === "/about-us" ? styles.navItemActive : styles.navItem} href="/about-us">
                        About Us
                    </Link>
                    <Link className={path === "/contact-us" ? styles.navItemActive : styles.navItem} href="/contact-us">
                        Contact Us
                    </Link>
                </div>
                <div className={styles.buttonContainer}>
                    <Link href={MAIN_LINK}>
                        <Button label="Download" isOutlined={true} />
                    </Link>
                </div>
                {!isNavOpen && (
                    <div
                        className={styles.iconContainer}
                        onClick={() => {
                            setIsNavOpen(true);
                        }}
                    >
                        <RxHamburgerMenu size={35} />
                    </div>
                )}
                {isNavOpen && (
                    <div className={styles.mobileNav}>
                        <div className={styles.navLinks}>
                            <Link className={path === "/" ? styles.navItemActive : styles.navItem} href="/">
                                Home
                            </Link>
                            <Link className={path === "/how-to-play" ? styles.navItemActive : styles.navItem} href="/how-to-play">
                                How To Play
                            </Link>
                            <Link className={path === "/about-us" ? styles.navItemActive : styles.navItem} href="/about-us">
                                About Us
                            </Link>
                            <Link className={path === "/contact-us" ? styles.navItemActive : styles.navItem} href="/contact-us">
                                Contact Us
                            </Link>
                            <Link href={MAIN_LINK}>
                                <Button label="Download Now" isOutlined={true} />
                            </Link>
                        </div>
                        <div
                            style={{ position: "absolute", top: "20px", right: "30px" }}
                            onClick={() => {
                                setIsNavOpen(false);
                            }}
                        >
                            <CgClose size={35} />
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
};

export default Navbar;
