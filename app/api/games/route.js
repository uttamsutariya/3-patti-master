import { connectDB } from "@/lib/mongodb";
import Game from "@/models/Game";
import { NextResponse } from "next/server";

export async function GET() {
	try {
		await connectDB();
		const games = await Game.find();
		return NextResponse.json(games);
	} catch (error) {
		console.log("Error ::", error);
		return NextResponse.json({ error: error.message }, { status: 500 });
	}
}

export async function POST(request) {
	try {
		await connectDB();
		const data = await request.json();
		const game = await Game.create(data);
		return NextResponse.json(game);
	} catch (error) {
		return NextResponse.json({ error: error.message }, { status: 500 });
	}
}
