import { connectDB } from "@/lib/mongodb";
import Game from "@/models/Game";
import { NextResponse } from "next/server";

export async function PUT(request, { params }) {
	try {
		await connectDB();
		const { id } = params;
		const data = await request.json();
		const game = await Game.findByIdAndUpdate(id, data, { new: true });
		return NextResponse.json(game);
	} catch (error) {
		return NextResponse.json({ error: error.message }, { status: 500 });
	}
}

export async function DELETE(request, { params }) {
	try {
		await connectDB();
		const { id } = params;
		await Game.findByIdAndDelete(id);
		return NextResponse.json({ message: "Game deleted successfully" });
	} catch (error) {
		return NextResponse.json({ error: error.message }, { status: 500 });
	}
}
