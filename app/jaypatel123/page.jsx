"use client";
import { useState, useEffect } from "react";
import styles from "./Admin.module.scss";

export default function AdminPanel() {
	const [games, setGames] = useState([]);
	const [formData, setFormData] = useState({
		name: "",
		downloadLink: "",
		imageUrl: "",
	});
	const [editingGame, setEditingGame] = useState(null);

	useEffect(() => {
		fetchGames();
	}, []);

	const fetchGames = async () => {
		const response = await fetch("/api/games");
		const data = await response.json();
		setGames(data);
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		if (editingGame) {
			await handleUpdate(e);
			return;
		}
		await fetch("/api/games", {
			method: "POST",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify(formData),
		});
		fetchGames();
		setFormData({ name: "", downloadLink: "", imageUrl: "", isSecure: true });
	};

	const handleUpdate = async (e) => {
		e.preventDefault();
		await fetch(`/api/games/${editingGame._id}`, {
			method: "PUT",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify(formData),
		});
		setEditingGame(null);
		fetchGames();
		setFormData({ name: "", downloadLink: "", imageUrl: "", isSecure: true });
	};

	const handleDelete = async (id) => {
		await fetch(`/api/games/${id}`, { method: "DELETE" });
		fetchGames();
	};

	const handleEdit = (game) => {
		setEditingGame(game);
		setFormData({
			name: game.name,
			downloadLink: game.downloadLink,
			imageUrl: game.imageUrl,
			isSecure: game.isSecure,
		});
	};

	return (
		<div className={styles.adminContainer}>
			<h1>Games Admin Panel</h1>

			<form onSubmit={handleSubmit} className={styles.form}>
				<input
					type="text"
					placeholder="Game Name"
					value={formData.name}
					onChange={(e) => setFormData({ ...formData, name: e.target.value })}
				/>
				<input
					type="text"
					placeholder="Download Link"
					value={formData.downloadLink}
					onChange={(e) => setFormData({ ...formData, downloadLink: e.target.value })}
				/>
				<input
					type="text"
					placeholder="Image URL"
					value={formData.imageUrl}
					onChange={(e) => setFormData({ ...formData, imageUrl: e.target.value })}
				/>
				<button type="submit">{editingGame ? "Update Game" : "Add Game"}</button>
				{editingGame && (
					<button
						type="button"
						onClick={() => {
							setEditingGame(null);
							setFormData({ name: "", downloadLink: "", imageUrl: "", isSecure: true });
						}}
					>
						Cancel Edit
					</button>
				)}
			</form>

			<div className={styles.gamesList}>
				{games.map((game) => (
					<div key={game._id} className={styles.gameItem}>
						<img src={game.imageUrl} alt={game.name} />
						<div className={styles.gameInfo}>
							<h3>{game.name}</h3>
							<p>Download Link: {game.downloadLink}</p>
							<p>Secure: {game.isSecure ? "Yes" : "No"}</p>
						</div>
						<div className={styles.gameActions}>
							<button onClick={() => handleDelete(game._id)}>Delete</button>
							<button onClick={() => handleEdit(game)}>Edit</button>
						</div>
					</div>
				))}
			</div>
		</div>
	);
}
