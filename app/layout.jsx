import { Inter } from "next/font/google";
import "./globals.css";
import Navbar from "@/components/Navbar/Navbar";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
    title: "Teen Patti Master",
    description: "Play Your Favourite Teen Patti Master Games Online",
    icons: {
        icon: "/assets/logo-master.jpg",
    },
    other: {
        "facebook-domain-verification": "da5wq84ymg0jowj0mc8289e7l9nr7t",
    },
    keywords: [
        "teen patti master apk",
        "3 patti master real money",
        "3 patti master apk download",
        "3 patti master",
        "tirupati master",
        "તીન પત્તી માસ્ટર",
        "3 patti master apk",
        "teen patti master",
        "slots meta apk",
        "teenpatti apk",
        "slot meta apk",
        "patti master",
        "teenpatti vungo apk",
        "tp master apk",
        "jhandi munda download",
        "3 patti logo",
        "all rummy application",
        "master 3 patti",
        "3 patti cash withdrawal phonepe",
        "teen patti live",
        "Teen Patti Master",
        "3 patti epic",
        "3patti epic",
        "teen patti apk download",
        "Teen Patti Master apk download",
        "Teen Patti Master real cash",
        "all teen patti",
        "all rummy",
        "teen patti",
        "teen patti gold",
        "teen patti",
        "teen patti gold",
        "teen patti king",
        "teen patti online",
        "teen patti master apk",
        "teen patti app",
        "teen patti rummy",
        "teen patti winner",
        "teen patti cash",
        "teen patti real",
        "teen patti sweet",
        "teen patti lucky",
        "teen patti vungo",
        "teen patti dhani",
        "teen patti pro",
        "teen patti jodi",
        "teen patti cash game",
        "3 patti queen",
        "3 patti 666",
        "3 patti queen download",
        "3 patti queen apk",
        "teen patti master download",
        "teen patti gold apk",
        "teen patti 100 bonus",
        "teen patti 100",
        "teen patti 100 withdrawal",
        "teen patti 100 500 bonus",
        "teen patti 101",
        "teen patti 10",
        "teen patti 11ic",
    ],
};

export default function RootLayout({ children }) {
    return (
        <html lang="en">
            <head>
                <noscript>
                    <img height="1" width="1" style={{ display: "none" }} src="https://www.facebook.com/tr?id=713962067388689&ev=PageView&noscript=1" />
                </noscript>
            </head>
            <body className={[inter.className]}>
                <Navbar />
                {children}
            </body>
        </html>
    );
}
