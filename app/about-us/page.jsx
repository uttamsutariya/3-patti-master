import Footer from "@/components/Footer/Footer";
import Hero from "@/components/Hero/Hero";
import PlayToEarn from "@/components/PlayToEarn/PlayToEarn";
import Social from "@/components/Social/Social";
import styles from "@/styles/About.module.scss";
import Image from "next/image";

const AboutUs = () => {
    return (
        <div className={["wrapper"]}>
            <div className={"backgroundImageContainer"}>
                <img src="/assets/bg.png" alt="" />
            </div>
            <Hero label={"About Us"} />
            <div className={styles.textContainer}>
                <p>
                    Step into the realm of Teen Patti Master, where the pursuit of seamless and just gaming experiences takes center stage. Our dedicated team toils tirelessly to
                    ensure that each visit to our platform transcends into an unparalleled gaming experience. Plenty of enthralling choices await you, from the classics like Teen
                    Patti and Rummy to the pulse-quickening adventure game and the mythical clash of dragon vs. tiger. Diversity is our forte, presenting gamers with a cornucopia
                    of options to discover their gaming nirvana
                </p>
                <p>
                    What sets us apart is our unwavering commitment to fairness and utmost transparency. We work hard to unveil the inner workings of every game, creating an
                    environment where every player can revel in the joy of an equitable playground. Offering a generous deposit bonus of up to 30%, a rarity among other similar
                    games, we motivate and empower our players with more value for their gaming endeavors. The nimble withdrawal process, containing the complexity of KYC
                    formalities, stands as a hallmark of convenience, ensuring swift access to your winnings.
                </p>
                <p>
                    Seamlessly cashing in on victories is a breeze for our players, with direct withdrawals from UPI identities and other financial conduits. As VIP agents, we
                    constantly explore novel avenues to aid our players in online income generation. Choosing our platform not only unlocks a treasure vault of gaming delights but
                    also presents a unique opportunity to kickstart a lucrative journey with minimal commitment. Welcome to Teen Patti Master, where every spin is a chance to
                    redefine your gaming destiny.
                </p>
            </div>
            <PlayToEarn
                buttonLabel={"Win Real Money"}
                labelWhite={"Game Teen Patti Master"}
                labelOrange={"Offers"}
                description={
                    "If your heart beats for card games like Bridge, Rummy, and Matka, then Teen Patti Game is your destined haven! Elevate your gaming experience to unparalleled heights with Teen Patti Master, where the thrill of free online gaming reaches its zenith. Engage in battles of wits against real players hailing from every corner of the globe in 3 Patti Epic, and carve your name as the undoubted master of this timeless classic. Immerse yourself in the diverse offerings of the 3 patti epic application, where the spectrum includes not only the beloved rummy and Teen Patti but also a plethora of other captivating games. The stakes are not just for fun; you have the chance to win real cash by indulging in any game and investing your money strategically. The cards are dealt, the bets are placed, and the virtual table awaits your skilful play."
                }
                rightChildComponent={<Image src="/assets/card-image.png" alt="Play to earn" width={400} height={400} />}
            />
            <Social />
            <Footer />
        </div>
    );
};

export default AboutUs;
