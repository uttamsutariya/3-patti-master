import Footer from "@/components/Footer/Footer";
import Hero from "@/components/Hero/Hero";
import Social from "@/components/Social/Social";
import styles from "@/styles/HowToPlay.module.scss";

const HowToPlay = () => {
    return (
        <div className={["wrapper"]}>
            <div className={"backgroundImageContainer"}>
                <img src="/assets/bg.png" alt="" />
            </div>
            <Hero label={"How To Play Teen Patti Master"} />
            <div className={styles.textContainer}>
                <p>
                    If you run into any issues while using the Teen Patti Master app, whether it’s about adding funds or making withdrawals, know that the app places a strong
                    emphasis on providing top-notch customer support. We have a professional team of experts available 24/7 to address your issues and complaints.
                </p>
                <p>
                    We offer a variety of customer services, including support options for recharges, which are at your fingertips. Customer satisfaction is our top priority, and
                    the Teen Patti Master app is prepared to assist you promptly.
                </p>
            </div>
            <div className={styles.howToPlayContainer}>
                <div>
                    <div className={styles.textDiv}>
                        <h1>Set Up</h1>
                        <ul>
                            <li>Dealer's picked randomly.</li>
                            <li>The Dealer gives three cards to every player, face down.</li>
                            <li>Extra cards go face down in the middle - that's the kitty.</li>
                        </ul>
                    </div>
                    <div className={styles.textDiv}>
                        <h1>Betting Time</h1>
                        <ul>
                            <li>The player to the dealer's left starts the betting. You can fold, match the bet, or raise it.</li>
                            <li>Fold and you're out, chips go to the kitty.</li>
                            <li>Raise if you're feeling bold.</li>
                            <li>Betting goes around until everyone's in or out.</li>
                        </ul>
                    </div>
                    <div className={styles.textDiv}>
                        <h1>Showdown</h1>
                        <ul>
                            <li>The last one standing after betting takes the kitty. If more players are in, cards are shown. The best hand grabs the kitty.</li>
                            <li>Jump into Teen Patti Master Mod apk, where games mean business. Click the play option and enjoy excellent gaming time!</li>
                        </ul>
                    </div>
                </div>
            </div>
            <Social />
            <Footer />
        </div>
    );
};

export default HowToPlay;
