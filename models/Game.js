import mongoose from "mongoose";

const GameSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: true,
		},
		downloadLink: {
			type: String,
			required: true,
		},
		imageUrl: {
			type: String,
			required: true,
		},
	},
	{
		timestamps: true,
	}
);

export default mongoose.models.Game || mongoose.model("Game", GameSchema);
